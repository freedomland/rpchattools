Methods = {}

local rphelp = "\nСписок rp команд:\
/me <text> - Отправить сообщение от 3 лица.\
/do <text> - Сказать что либо от 3 лица (например: Убью этого гуара, все равно эти ботинки испорчены.).\
/todo <text> - Сказать что либо от 3 лица повествованием (например: Иди сюда, раб! ^ этому каждиту).\
/try <text> - Попытаться что-то сделать (например подбросить монетку, будет отражена успешность или провал действия).\
/speech <type> <index> - Воспроизводит определенную речь доступную для вашей расы. (/s)\
/anim <animation> - Воспроизводит доступные анимации для вашего персонажа. (/a)\
/sarcasm <text> - Ну я думаю это не надо объяснять? :) (/scm)"

local rphelpadmin = "\n/eac on/off - Включить/отключить глобальный чат.\
/mute <pid> on/off - Обезмолвить/разрешить говорить игроку.\
/setnamecolor <pid> <цвет> - Установить цвет для конкретного игрока. Не применимо к администраторам и модераторам."

local messageFile = os.getenv("MOD_DIR") .. "/Messages/logs.txt"
local localChatCellRadius = 1 -- 0 means only the players in the same cell can hear eachother
local globalChatEnable = true

local styles = {}
local playerStyles = {}

function Methods.Init()
	
	local home = os.getenv("MOD_DIR").."/style/"
    local file = io.open(home .. "prefix.json", "r")
	if file ~= nil then
		io.close()
		styles = jsonInterface.load("style/prefix.json")
	else
		styles.admin = color.Red.."[Admin]"..color.Default
		styles.moderator = color.BlueViolet.."[Mod]"..color.Default
		
		jsonInterface.save("style/prefix.json", styles)
	end

end

function SendMessageToAllInCell(cellDescription, message)
	for index,pid in pairs(LoadedCells[cellDescription].visitors) do
		if Players[pid].data.location.cell == cellDescription then
			tes3mp.SendMessage(pid, message, false)
		end
	end
end

function ChatLog(pid, cmd)
	local name = tes3mp.GetName(pid)
	local ip = Players[pid].data.ipAddresses

	local file = io.open(messageFile, "a")
	file:write(os.date("[ %X | %x | ", os.time()) .. ip[#ip] .. "  ] " .. name .. ": " .. cmd .. "\n")
	file:close()
end

function loadPlayerStyle(pid)
	-- Replace characters not allowed in filenames
    local acc = string.upper(Players[pid].name)
    acc = string.gsub(acc, patterns.invalidFileCharacters, "_")
    acc = acc .. ".json"
	
	local style = {}
	style.nameColor = color.Default
	style.prefix = {}
	
	local home = os.getenv("MOD_DIR").."/style/players/"
    local file = io.open(home .. acc, "r")
	if file ~= nil then
		io.close()
		style = jsonInterface.load("style/players/"..acc)
	else
		style.nameColor = color.Default
		style.prefix = {}
		jsonInterface.save("style/players/"..acc, style)
	end
	
	return style
end

function ShowHelp(pid)
	local admin = Players[pid]:IsAdmin()
	
	if admin then
		tes3mp.CustomMessageBox(pid, -1, rphelp .. rphelpadmin .. "\n", "Ok")
	else
		tes3mp.CustomMessageBox(pid, -1, rphelp .. "\n", "Ok")
	end
end

function currentTime()
	local t = os.date ("*t")
	local str = "[" .. tostring(t.hour) .. ":" .. tostring(t.min) .. "] "
	return str
end

function savePlayerStyle(pid)
	-- Replace characters not allowed in filenames
    local acc = string.upper(Players[pid].name)
    acc = string.gsub(acc, patterns.invalidFileCharacters, "_")
    acc = acc .. ".json"
	
	jsonInterface.save("style/players/"..acc, playerStyles[pid])
end

function SendGlobalMessage(pid, message, prefix, useName)
	if useName == true then
		tes3mp.SendMessage(pid, currentTime() .. prefix .. GetColorName(pid)..": "..message.."\n", true)
	else
		tes3mp.SendMessage(pid, currentTime() .. prefix .. message.."\n", true)
	end
end

function SendLocalMessage(pid, message, useName)
	local playerName = Players[pid].name
	
	-- Get top left cell from our cell
	local myCellDescription = Players[pid].data.location.cell
	
	if tes3mp.IsInExterior(pid) == true then
		local cellX = tonumber(string.sub(myCellDescription, 1, string.find(myCellDescription, ",") - 1))
		local cellY = tonumber(string.sub(myCellDescription, string.find(myCellDescription, ",") + 2))
		
		local firstCellX = cellX - localChatCellRadius
		local firstCellY = cellY + localChatCellRadius
		
		local length = localChatCellRadius * 2
		
		for x = 0, length, 1 do
			for y = 0, length, 1 do
				-- loop through all y inside of x
				local tempCell = (x+firstCellX)..", "..(firstCellY-y)
				-- send message to each player in cell
				if LoadedCells[tempCell] ~= nil then
					if useName then
						SendMessageToAllInCell(tempCell, currentTime() .. GetColorName(pid)..": "..message.."\n")
					else
						SendMessageToAllInCell(tempCell, currentTime() .. message.."\n")
					end
				end
			end
		end
	else
		if useName then
			SendMessageToAllInCell(myCellDescription, currentTime() .. GetColorName(pid)..": "..message.."\n")
		else
			SendMessageToAllInCell(myCellDescription, currentTime() .. message.."\n")
		end
	end
end

function GetColorName(pid)
	local playerName = Players[pid].name
	
	local prefix = ""
	for i,tag in pairs(playerStyles[pid].prefix) do
		if styles[tag] ~= nil then
			prefix = prefix..styles[tag].." "
		end
	end
	
	return prefix..playerStyles[pid].nameColor..playerName..color.Default
end

function Methods.CheckPlayerMuted(pid)
	local muted = false
	
	if Players[pid].data.customVariables.muted == true then
		muted = true
	end

	if muted then
		tes3mp.SendMessage(pid, "#FF0000Вы были обезмолвлены и не можете писать в чат пока вам снова не разрешат.\n", false)
		return true
	end
	
	return false
end

---------------------------------------------------------
--				Start of RP functions				   --
---------------------------------------------------------

function AddPrefix(pid, targetPid, description)

	if styles[description] ~= nil then
		for i,d in pairs(playerStyles[targetPid].prefix) do
			if d == description then
				tes3mp.SendMessage(pid, "#FF0000Этот игрок уже имеет префикс \""..description.."\".\n", false)
			end
		end
		
		table.insert(playerStyles[targetPid].prefix, description)
		
		savePlayerStyle(targetPid)
		
		tes3mp.SendMessage(pid, "#FF0000Префикс \""..description.."\" добавлен к "..GetColorName(targetPid)..".\n", false)
		
	else
		tes3mp.SendMessage(pid, "#FF0000Префикс \""..description.."\" не существует.\n", false)
	end

end

function RemovePrefix(pid, targetPid, description)

	if styles[description] ~= nil then
	
		local index = 0
		
		for i,d in pairs(playerStyles[targetPid].prefix) do
			if d == description then
				index = i
			end
		end
		
		if index > 0 then
			table.remove(playerStyles[targetPid].prefix, index)
		end
		
		savePlayerStyle(pid)
		
		tes3mp.SendMessage(pid, "Удалён префикс \""..description.."\" игрока "..GetColorName(targetPid)..".\n", false)
		
	else
		tes3mp.SendMessage(pid, "Префикс \""..description.."\" не существует.\n", false)
	end

end

function SetNameColor(pid, color)
	if string.len(color) == 7 then
		if string.byte(string.sub(color, 1, 1)) == 35 then
			for i=2,7,1 do
				local b = string.byte(string.sub(color, i, i))
				if (b < 48 or b > 57) and (b < 65 or b > 70) then
					tes3mp.SendMessage(pid, "Неверный цвет, должен быть 0-9 / A-F (например: FFFFFF).\n", false)
				end
			end
			
			if playerStyles[pid] ~= nil then
				playerStyles[pid].nameColor = color
				savePlayerStyle(pid)
				tes3mp.SendMessage(pid, "#0000FFЦвет ника "..GetColorName(pid).." был установлен.\n", false)
			else
				tes3mp.SendMessage(pid, "#FF0000Игрок "..pid.." не существует.\n", false)
			end
		end
		tes3mp.SendMessage(pid, "#FF0000Неверный цвет, должен быть 0-9 / A-F (например: FFFFFF).\n", false)
	else
		tes3mp.SendMessage(pid, "#FF0000Цвет должен состоять из 6 символов (например: FFFFFF).\n", false)
	end

end

function Methods.OnPlayerSendCommand(pid, cmd, message)

	local admin = Players[pid]:IsAdmin()

	if cmd[1] == "message" or cmd[1] == "msg" or cmd[1] == "w" then
		if pid == tonumber(cmd[2]) then
			tes3mp.SendMessage(pid, "Вы не можете отправлять сообщение себе.\n", false)
		elseif cmd[3] == nil then
			tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n", false)
		elseif myMod.CheckPlayerValidity(pid, cmd[2]) then
			local targetPid = tonumber(cmd[2])
			local targetName = GetColorName(targetPid)
			message = GetColorName(pid) .. " #ED00FF[шепчет]#FFFFFF: "
			local messageYour = "Вы #ED00FFнашептали#FFFFFF " .. targetName .. ": "
			message = message .. tableHelper.concatenateFromIndex(cmd, 3) .. "\n"
			messageYour = messageYour .. tableHelper.concatenateFromIndex(cmd, 3) .. "\n"
			tes3mp.SendMessage(pid, messageYour, false)
			tes3mp.SendMessage(targetPid, message, false)
		end
            
		ChatLog(pid, message)
		
		return true
		
	elseif cmd[1] == "me" then
		if cmd[2] == nil then
			tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n")
		else
			local message = "#927eaf* " .. GetColorName(pid) .. " #927eaf" .. tableHelper.concatenateFromIndex(cmd, 2) .. " *#FFFFFF"
			SendLocalMessage(pid, message, false)
		end
			
		ChatLog(pid, message)
		
		return true
            
	elseif cmd[1] == "all" or cmd[1] == "a" then
		if globalChatEnable == false and not admin then
			tes3mp.SendMessage(pid, "#FF0000Глобальный чат был отключён администратором.\n#FFFFFF")
		else
			if cmd[2] == nil then
				tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n")
			else
				SendGlobalMessage(pid, "#FFFFFF" .. tableHelper.concatenateFromIndex(cmd, 2), "#90EE90[всем] ", true)
			end
		end
			
		ChatLog(pid, message)
		
		return true
			
	elseif cmd[1] == "do" then
		if cmd[2] == nil then
			tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n")
		else
			local message = "#927eaf* " .. tableHelper.concatenateFromIndex(cmd, 2) .. " #FFFFFF| " .. GetColorName(pid)
			SendLocalMessage(pid, message, false)
		end
			
		ChatLog(pid, message)
		
		return true
			
	elseif cmd[1] == "todo" then
		local tmp = tableHelper.concatenateFromIndex(cmd, 2):split("^")
			
		if tmp[1] == nil or tmp[2] == nil then
			tes3mp.SendMessage(pid, "Неверное использование команды /todo, введите /help rp чтобы получить справку.\n", false)
		else
			local message = "#FFFFFF* " .. tmp[1] .. "* " .. "#927eafСказал " .. GetColorName(pid) .. "#927eaf, " .. tmp[2] .. "."
			SendLocalMessage(pid, message, false)
		end
			
		ChatLog(pid, message)
		
		return true
			
	elseif cmd[1] == "try" then
		if cmd[2] == nil then
			tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n")
		else
			local message = "#927eaf* " .. GetColorName(pid) .. " #927eaf" .. tableHelper.concatenateFromIndex(cmd, 2) .. " *"
			math.randomseed( tonumber(tostring(os.time()):reverse():sub(1,2)) )
			if math.random(1,2) == 1 then
				message = message .. " #36FF00(упешно)#FFFFFF"
			else
				message = message .. " #FF1100(неудачно)#FFFFFF"
			end
			
			SendLocalMessage(pid, message, false)
		end
			
		ChatLog(pid, message)
		
		return true
			
	elseif cmd[1] == "sarcasm" or cmd[1] == "scm" then
		if globalChatEnable == false and not admin then
			tes3mp.SendMessage(pid, "#FF0000Глобальный чат был отключён администратором.\n#FFFFFF")
		else
			if cmd[2] == nil then
				tes3mp.SendMessage(pid, "Вы не можете отправить пустое сообщение.\n")
			else
				SendGlobalMessage(pid, "#FFFFFF" .. tableHelper.concatenateFromIndex(cmd, 2), "#927eaf[сарказм] ", true)
			end
		end
			
		ChatLog(pid, message)
			
		return true
		
	elseif cmd[1] == "eac" and cmd[2] ~= nil and admin then
		if cmd[2] == "on" then
			globalChatEnable = true
			tes3mp.SendMessage(pid, "#00FF00Глобальный чат был включён!\n", true)
		elseif cmd[2] == "off" then
			globalChatEnable = false
			tes3mp.SendMessage(pid, "#FF0000Глобальный чат был отключен!\n", true)
		else
			tes3mp.SendMessage(pid, "#FF0000Неверный аргумент! /eac принимает только on и off.\n", false)
		end
			
		return true
			
	elseif cmd[1] == "mute" and cmd[2] ~= nil and cmd[3] ~= nil and admin then
		local targetPid = tonumber(cmd[2])
			
		if targetPid == pid then
			tes3mp.SendMessage(pid, "#FF0000Вы не можете обезмолвить себя!\n", false)
			return false
		end
			
		if cmd[3] == "on" then
			Players[targetPid].data.customVariables.muted = true
			tes3mp.SendMessage(pid, "#FF0000" .. Players[targetPid].accountName .. " был обезмолвлен!\n", true)
		elseif cmd[3] == "off" then
			Players[targetPid].data.customVariables.muted = false
			tes3mp.SendMessage(pid, "#00FF00" .. Players[targetPid].accountName .. " снова может говорить!\n", true)
		else
			tes3mp.SendMessage(pid, "#FF0000Неверный аргумент! /mute принимает только on и off.\n", false)
		end
		
		return true
		
	elseif cmd[1] == "help" and cmd[2] == "rp" then
		ShowHelp(pid)
		return true	
		
	--[[elseif cmd[1] == "setprefix" and admin then
		if not Players[tonumber(cmd[2])]:IsAdmin() then
			AddPrefix(pid, tonumber(cmd[2]), cmd[3])
		else
			tes3mp.SendMessage(pid, "#FF0000Нельзя изменять/добавлять префиксы и цвета админам или модераторам!\n", false)
		end
		return true
	
	elseif cmd[1] == "removeprefix" and admin then
		if not Players[tonumber(cmd[2])]:IsAdmin() then
			RemovePrefix(tonumber(cmd[2]), cmd[3])
		else
			tes3mp.SendMessage(pid, "#FF0000Нельзя изменять/добавлять префиксы и цвета админам или модераторам!\n", false)
		end
		return true]]
		
	elseif cmd[1] == "setnamecolor" and admin then
		if not Players[tonumber(cmd[2])]:IsAdmin() then
			SetNameColor(tonumber(cmd[2]), cmd[3])
		else
			tes3mp.SendMessage(pid, "#FF0000Нельзя изменять/добавлять цвета админам или модераторам!\n", false)
		end
		return true
		
	end
		
	return false
end

function Methods.OnPlayerSendMessage(pid, message)
	SendLocalMessage(pid, message, true)
	ChatLog(pid, message)
end

function Methods.OnPlayerConnect(pid)
	playerStyles[pid] = loadPlayerStyle(pid)
end

function Methods.OnPlayerDisconnect(pid)
	playerStyles[pid] = nil
end

return Methods
