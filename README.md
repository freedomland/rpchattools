# rpchattools

Fork of [RpChatEssentials](https://github.com/David-AW/tes3mp-roleplay-chat-essentials) to suit Freedom Land's needs.

### What the different

- Remove all unnecessary (for our server) code. 
- Simplify the plugin itself. Get rid of msgScriptEssentials.
- Move all commands to one function. In fact, move all chat-related commands from server.lua.
- Add roleplay commands such as /do, /todo, /try; modify /me.
- Ability for admins to disable and enable global chat.
- Ability for admins to mute specific player.
- Ability for admins to setup player color.
- Display chat time.
- By default enable local chat.
- Global chat available by /a or /all.
- Chat log, even /msg (/w, /message). (store in CoreScripts/data/Messages/log.txt, can be disabled)


### How to install

- Add in top of server.lua [ rpChatTools = require("rpChatTools") ]
- In [ function OnServerInit() ] after [ LoadPluginList() ] add [ rpChatTools.Init() ]
- In [ OnPlayerConnect(pid) ] after [ myMod.OnPlayerConnect(pid, playerName) ] add [ rpChatTools.OnPlayerConnect(pid) ]
- In [OnPlayerDisconnect(pid) ] after [ myMod.OnPlayerDisconnect(pid) ] add [ rpChatTools.OnPlayerDisconnect(pid) ]
- After [ local cmd = ... ] add 	
	```
		if not rpChatTools.CheckPlayerMuted(pid) and rpChatTools.OnPlayerSendCommand(pid, cmd, message) then
			return false
		end
	```
- In very end of [ OnPlayerSendMessage ] before end add
	```
		if not rpChatTools.CheckPlayerMuted(pid) then
			rpChatTools.OnPlayerSendMessage(pid, message)
		end
	```
- In plugin itself you can change options to suite your server
